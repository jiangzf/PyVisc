#/usr/bin/env python
#author:lgang
#email: lgpang@qq.com
#create time :2019
'''cacl the Lambda polarization on the freeze out hypersurface'''

from __future__ import absolute_impoer, division, print_function
import numpy as np
import os
import sys
from time import time
import math
import four_momentum as mom

import pyopencl as cl
from pyopencl.array import Array
import pyopencl.array as cl_array


os.eviron['PYOPENCL_CTX']=':1'

class Polarization(object):
    ''' The pyopencl version for lambda polarisation,
    initialize with freeze out hyper surface and omega^{mu}
    on freeze out hyper surface.'''
    def __init__(self, sf, omega):
        ''' Param:
              sf: the freeze out hypersf ds0, ds1, ds2, ds3, vx, vy, veta, etas
              omega: omega^{tau}, x, y, etas
        '''
    self.cwd, cwf = os.path.split(__file__)
    self.ctx = cl.create_some_context()
    self.queue = cl.CommandQueue(self.ctx)
    
    # calc umu since they are used for each (Y, pt, phi)
    self.size_sf = len(sf[:,0])
    
    h_sf = sf.astype(np.float32)
    h_omega = omega.astype(np.float32)
    print(h_omega)
    
    mf = cl.mem_flags
    self.d_sf = cl.Buffer(self.ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=h_sf)
    self.d_omega = cl.Buffer(self.ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=h_omega)
    
    
    def get(self, momentum_list, title=1):
        '''Return: polarization, density at given momentum
        Params:
           :param momentum: a numpy array with shape (size, 4)
           where size = number of different four-momentum vector
           in each vector, pt=(mt, Y, px, py)
           :param title: num of calculations in each workgroup '''
        d_momentum = cl_array.to_device(self.queue, 
                                        momentum_list.astype)
                                        
        num_of_mom = len(momentum_list)
        
        print("num_of_mom=", num_of_mom)
        
        compile_options = ['-D num_of_mom=%s'%num_of_mom, '-D title=%s'%tilte]
        
        cwd, cwf = os.path.split(__file__)
        
        block_size = 256
        compile_options = ['-D BSZ=%s'%block_size]
        compile_options.append('-I '+os.path.join(cwd, '../kernel/'))
        complie_options.append('-D USE_SINGLE_PRECISION')
        
        fpath = os.path.join(cwd, '../kernel/kernel_polarization.cl')
        
        with open(fpath, '')