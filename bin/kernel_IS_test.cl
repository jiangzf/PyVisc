#include<helper.h>

constant real gmn[4][4] = {{1.0f, 0.0f, 0.0f, 0.0f},
                           {0.0f,-1.0f, 0.0f, 0.0f},
                           {1.0f, 0.0f,-1.0f, 0.0f},
                           {1.0f, 0.0f, 0.0f,-1.0f}};
                           
// use pimn and ev at i-2, i-1, i, i+1, i+2 to calc src term from flux
// pr_mh = pr_{i-1/2} and pr_ph = pr_{i+1/2}
// there mh, ph terms are calculated 1 time and used 10 times by pimn
real kt1d_real(
       real Q_im2, real Q_iml, real Q_i, real Q_ipl, real Q_ip2,
       real v_mh, real v_ph, real lam_mh, real lam_ph,
       real tau, int along)
{
    real DA0, DA1;
    DA0 = minmod(0.5f * (Q_ipl-Q_iml),
            minmod(THETA*(Q_ip1-Q_i), THETA*(Q_i-Q_iml)));
            
    DA0 = minmod(0.5f * (Q_ip2-Q_i),
            minmod(THETA*(Q_ip2-Q_ip1), THETA*(Q_ipl-Q_i));
            
    real AL = Q_i + 0.5f * DA0;
    real AR = Q_ipl - 0.5f *DA1;
    
    // Flux Jp = (Q + pr*g^{tau mu})*v^{x} - pr*g^{x mu}
    real Jp = AR * v_ph;
    real Jm = AL * v_ph;
    
    //first part of kt1d; the final results = src[i] - src[i-1]
    real src = 0.5f * (Jp+Jm) - 0.5f*lam_ph*(AR-AL);
    
    DA1 = DA0; //reuse the previous calculate value
    DA0 = minmod(0.5f*(Q_i - Q_im2),
            minmod(THETA*(Q_i - Q_iml), THETA*(Q_iml - Q_im2)));
            
    AL = Q_iml + 0.5f * DA0;
    AR = Q_i - 0.5f * DA1;
    
    Jp = AR * v_mh;
    Jm = AL * v_mh;
    
    // second part of kt1d; final results = src[i] -  src[i-1]
    src -= 0.5f*(Jp+Jm) - 0.5f*lam_mh*(AR-AL);
    
    return src;
}  


// initialize d_pil and d_udiff between u_ideal* and u_visc
__kernel void visc_initialize(
            __global real * d_pil,
            __global real * d_goodcell,
                    __global real4 * d_udiff,
                    __global real4 * d_ev,
                    const real tau,
                read_only image2d_t eos_table){
    int I = get_globat_id(0);
    }