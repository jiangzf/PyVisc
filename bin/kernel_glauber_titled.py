#include<helper.h>

real Nw(real Ta, real Tb); # WN 函数， 由 T1 和 T2 确定， T1 和 T2 由横平面坐标点 x 和 y 确定
real Nb(real Ta, real Tb);  # Binary 二次碰撞函数 NumofBinaryCollision, 同样由 TA 和 TB 确定 
real ed_transverse(real Ta, real Tb);
real weight_along_eta(real z, real etas0_);
real thickness(real x, real y);

// needs Ro0, R, Eta from gpu_define # 由主函数 GPU 传递过来的参数，包括 Ro0, R, 以及 Eta 
real thickness(real x, real y)  # 厚度函数T(x,y), 横平面的坐标点x y 确定,D.E. (B1)
{
            real r, thickness, f, z, cut, a[10], b[10];
            real zk[5] = {-0.9061798f, -0.5386492f, 0.0f, 0.5386493f, 0.9061798f};
        //range(-1,1)
        
            real Ak[5]={0.2369269f, 0.4786287f, 0.568889f, 0.4786287f, 0.2369269f};
        //weight
        
        thickness = 0.0f;
        cut = 5.0f*R;
        for ( int j=0; j!=10; j++){
             a[j] = j*cut / 10.0f;
             b[j] = (j+1)*cut / 10.0f;
             for (int i=0; i!=5; ++i){
                      z=(a[j]+b[j]/2.0f+(b[j]-a[j])/2.0f* zk[i];) 
                      r=sqrt( x*x+y*y+z*z );
                      f=Ro0/(1.0f+exp((r-R)/Eta));
                      thickness= thickness + Ak[i]*f*(b[j]-a[j])/2.0f; # 厚度函数
             }
        }
        return 2.0f*thickness;
}

// energy deposition along eta, define BJORKEN_SCALING to set heta=1
real weight_along_eta(real z, real etas0_){
        real heta;
#ifdef BJORKEN_SCALING
    heta = 1.0f
#else
        if (fabs(z-etas0_) > Eta_flat ){
        heta=exp(-pow(fabs(z-etas0_)-Eta_flat,2.0f)/(2.0f*Eta_gw*Eta_gw));  # 纵向的函数，由三个变量决定 z, etas0, Eta_flat      
        } else {
            heta = 1.0f;
        }
#endif
        return heta; # 根据需求来进行定义
}

// energy deposition in transverse plane from 2 components modedl # 能量沉积到横平面通过两个部分模型
// (knw*nw(x,y) + knb*nb(x,y))*heta, b is impact parameter # (knw*nw(x,y) + knb*nb(x,y)) * heta
real ed_transverse(real Ta, real Tb) { 
    return (Hwn*Nw(Ta, Tb) + (1.0f-Hwn)*Nb(Ta, Tb));
} # 定义能量横平面函数， 由 TA 及 TB 确定， 返回函数 (Hwn*Nw(Ta, Tb) + (1.0f-Hwn)*Nb(Ta, Tb)), 注意 Hwn 是一个小于1的数


// the rapidity twist due to forward-backward assymetry  # 快度 前后不对称
// where Ta = T- =T(x-b/2, y) and Tb = T+ = T(x+b/2, y)  # Ta = T- = T(x-b/2, y) and Tb = T+ = T(x+b/2, y)
inline real etas0(real Ta, real Tb) {                    # etas0函数， 由 TA 及TB 所确定
      // gamma_n = sqrt_s / (2*m_nucleon)
      real gamma_n = SQRTS / (2.0f * 0.938f);            # gamma_beam 
      real v_n = sqrt(gamma_n*gamma_n - 1.0f)/gamma_n;   # beta_beam
      real T1 = (Ta + Tb)*gamma_n;          # T1 = (Ta+Tb)*gamma_beam 
      real T2 = (Ta - Tb)*gamma_n*v_n;      # T2 = (Ta+Tb)*gamma_beam*beta_beam
      // return 0.5f*log((T1+T2)/(T1-T2))   
      return 0.0f;                          # 返回 0.0f, 或者 0.5* log((T1+T2)/(T1-T2)), 就是etas0的值，通常对称，则设为0，如果不对称，则要从新设定
}

// number of wounded nucleons  # WN  函数，由A,B核确定，里面的Si0 是质子-质子截面
real Nw(real Ta, real Tb){
       return Ta*(1.0f - pow(1.0f - Si0*Tb/NumOfNucleons, NumOfNucleons))
                  Tb*(1.0f-pow(1.0f-Si0*Ta/NumOfNucleons, NumOfNucleons));
}

// number of binary collisions # 二次碰撞数目 NBC 函数
real Nb(real Ta, real Tb){
        return Si0*Ta*Tb;
}


// each thread calc one slice of initial energy density with fixed
// (x, y) and varying eta_s # 确定横平面 x,y 坐标下， 变换纵坐标，每根线所计算的初态能量密度
__kernel void glauber_ini( __global real4 * d_ev1 )   # 初态能量密度是一个 4 矢量, 由GPU整体调用核心，kernel, 包括x,y方向循环
{
    int i = get_global_id(0);   # 定义 i, 来自整体变量 
    int j = get_global_id(1);   # 定义 j
    real x = (i - NX/2)*DX;     # x = (i - NX/2)*DX
    real y = (j - NY/2)*DY;     # y = (j - NY/2)*DY
    real Tcent = thickness(0.0f, 0.0f); # Tcent = thickness(0.0f, 0.0f)
    real ed_central = ed_transverse(Tcent, Tcent); # energy density ed_Central = ed_transverse(Tcent, Tcent)
    
    real kFactor = Edmax / ed_central; # KFactor = Edmax/ed_central, Edmax 来自实验对比确定
    real b = ImpactParameter;  # 碰撞参量
    real Ta = thickness(x-0.5f*b, y); # Ta 
    real Tb = thickness(x+0.5f*b, y);  # Tb
    real edxy = kFactor*ed_transeverse(Ta, Tb);
    real etas0_ = etas0(Ta, Tb);
    for ( int k = 0; k < NZ; k++) {
        real etas = (k - NZ/2)*DZ;
        real heta = weight_along_eta(etas, etas0_);
        d_ev1[i*NY*NZ + j*NZ + k] = (real4)(edxy*heta, 0.0f, 0.0f, 0.0f);
    } # 这里的循环处理横向的能量密度分布 edxy*heta, 0.0f
}

__kernel void num_of_binary_collisions( __global real * d_nbinary,
                                        const real xlow,
                                        const real ylow,
                                        const real dx,
                                        const real dy,
                                        const int nx, 
                                        const int ny)
{
    int i = get_global_id(0);
    int j = get_global_id(1);
    real x = xlow + i*dx;
    real y = ylow + j*dy;
    
    real b = ImpactParameter;
    real Ta = thickness(x-0.5f*b, y);
    real Tb = thickness(x+0.5f*b, y);
    d_nbiary[i*ny + j] = Nb(Ta, Tb);
}      
