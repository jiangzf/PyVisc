#ifndef  __HELPERH__
#define  __HELPERH__

#include"real_type.h"
#include"eos_table.h"

#define THETA 1.1f
#define hbarc 0.19733f

//represent 16 components of pi^{mu nu} in 10 elements
#define idx(i, j) (((i)<(j))?((7*(i)+2*(j)-(i)*(i))/2):((7*(j)+2*(i)-(j)*(j))/2))

// idn() return the idx of pi^{mu nu}_{i,j,k} in global mem
// I = i*NY*NZ + j*NZ + k
#define idn(I, mn)(I)*10 + mn


#define ALONG_X 0
#define ALONG_Y 1
#define ALONG_Z 2  

// parametrization of temperature dependent eta/s
inline real etaos(real temperature){
    return (temperature < ETAOS_XMIN) ? (ETAOS_LEFT_SLOP * (temperature - ETAOS_XMIN) + ETAOS_YMIN)
                                      : (ETAOS_RIGHT_SLOP * (temperature - ETAOS_XMIN)+ ETAOS_YMIN);
}

// kt1d to calc H(i+1/2)-H(i-1/2), along=0,1,2 for x, y, z
real4 kt1d(real4 ev_im2, real4 ev_im1, real4 ev_i, real4 ev_ip1, real4 ev_ip2,
           real tau, int along, read_only image2d_t eos_table);

// g^{tau, mu}, g^{x, mu}, g^{y, mu}, g^{eta, mu} without tau*tau
constant real4 gm[4] = 
{(real4)(1.0f, 0.0f, 0.0f, 0.0f),
(real4)(0.0f, -1.0f, 0.0f, 0.0f),
(real4)(0.0f, 0.0f, -1.0f, 0.0f),
(real4)(0.0f, 0.0f, 0.0f, -1.0f)};
///////////////////////////////////////////////////
/*!Calc gamma for vx, vy, vz where vz=veta in Miline space */
inline real gamma(real vx, real vy, real vz){
    
}