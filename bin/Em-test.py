# -*- coding: utf-8 -*-
"""
Created on Sat Nov 30 15:36:18 2019

@author: Jiang-Zefang
"""

from __future__ import absolute_import, division, print_function
import numpy as np
from scipy import integrate
import os
import sys
from time import time
import math
import matplotlib.pyplot as plt 
from matplotlib.ticker import NullFormatter # useful for `logit` scale

sigma1 = 0.023 # electrical conductivity fm^{-1}
sigma2 = 0.00001
Yb = 8.0  # Yb = 5.4 RHIC and Yb = 8.0 for LHC
alpha = 1.0/137.0 # electromagnetic coupling constant  
Z = 82 # Z = 79 for Au and Z = 82 for Pb
R = 7  # radius 7 fm
b = 7.0 # impact parameter

# the Delta function
def Delta(tau, eta, xperp1, xperp2, phi1, phi2):
    #xperp1 -> x_{perp}, xperp2 -> x_{perp2}‘
    #phi1 -> phi, phi2 - > phi'
    Delta1 = tau*tau*np.sinh(Yb-eta)*np.sinh(Yb-eta)+xperp1*xperp1\
    +xperp2*xperp2-2.0*xperp1*xperp2*np.cos(phi1-phi2)
    return Delta1

# the A function
def Afun(tau, eta, sigma, xperp1, xperp2, phi1, phi2):   
    Afunc = (sigma/2.0)*(tau*np.sinh(Yb)*np.sinh(Yb-eta)\
           -np.abs(np.sinh(Yb))\
           *np.sqrt(Delta(tau, eta, xperp1, xperp2, phi1, phi2)))
    return Afunc
    
# the eBx plus function
def eByp(tau, eta, sigma, xperp1, xperp2, phi1, phi2):
    ebyp = alpha * np.sinh(Yb)*(xperp1*np.cos(phi1)-xperp2*np.cos(phi2))\
          *((sigma/2.0)*np.abs(np.sinh(Yb))\
          *np.sqrt(Delta(tau, eta, xperp1, xperp2, phi1, phi2)))\
          *np.exp(Afun(tau, eta, sigma, xperp1, xperp2, phi1, phi2))\
          /pow(Delta(tau, eta, xperp1, xperp2, phi1, phi2),3.0/2.0)
    return ebyp

def rhop(xperp1, phi1):
    rhop1 = (3.0/(2.0*np.pi*R*R*R))\
            *np.sqrt(R*R - (xperp1*xperp1+b*xperp1*np.cos(phi1)+b*b/4.0))
    return rhop1
        
def rhom(xperp1, phi1):
    rhom1 = (3.0/(2.0*np.pi*R*R*R))\
            *np.sqrt(R*R - (xperp1*xperp1+b*xperp1*np.cos(phi1)+b*b/4.0))
    return rhom1

def xin(phi2):
    xin1 = -(b/2.0)*(np.cos(phi2))\
           +np.sqrt(R*R - (b*b/4.0)*np.sin(phi2)*np.sin(phi2))
    return xin1

def xout(phi2):
    xout1 = (b/2.0)*(np.cos(phi2))\
           +np.sqrt(R*R-(b*b/4.0)*np.sin(phi2)*np.sin(phi2))
    return xout1


def ebsy1(xperp2, phi2,tau, sigma):
    xperp1 = 0.0
    eta = 0.0
    phi1 = 0.0
    ebyo = -Z * xperp2*rhom(xperp1, phi1)\
    *(eByp(tau, eta,sigma, xperp1, xperp2, np.pi-phi1, phi2)\
    +eByp(tau, -eta,sigma, xperp1, xperp2, phi1, phi2))
    return ebyo

tau_step = 0.02
ebys = []
ebys_sigma2 = []
t = []
y1 = []
y2 = []

for i in range(0, 101, 1):
    tau = i * tau_step  
    ebyso1, err = integrate.dblquad(ebsy1,-np.pi/2.0, np.pi/2.0,xin,xout,args=(tau,sigma1))        
    ebys.append((tau, ebyso1))
    t.append(tau)
    y1.append(ebyso1)
    ebyso2, err = integrate.dblquad(ebsy1,-np.pi/2.0, np.pi/2.0,xin,xout,args=(tau,sigma2))
    y2.append(ebyso2)
#print (x)
#print (y)
plt.plot(t,y1,color='blue', label = r'$\sigma=0.023$ fm$^{-1}$')
plt.plot(t,y2,color='red', label = r'$\sigma=0.000$ fm$^{-1}$')

plt.yscale('log')
plt.xlabel(r"$\tau$[fm]",fontsize=20) 
plt.ylabel(r"$eB_{y}$[fm$^{-2}$]",fontsize=20) 
plt.xlim(0.0001,2.0)
plt.ylim(1e-6, 1)
plt.xticks(fontsize=10)
plt.yticks(fontsize=10)

plt.legend()
plt.savefig('EM-sigma.pdf')
plt.show()