#this file is edited by zefang



import numpy as np
from subprocess import call
import os
from time import time
from glob import glob
import pyopencl as cl
import matplotlib.pyplot as plt
import h5py

import os,sys
cwd, cwf = os.path.split(__file__)
print('cwd=, cwd')

sys.path.append(os.path.join(cwd, '../pyvisc'))
from config import cfg, write_config '''同目录下，从config 文件中导入vcfg模块'''
from visc import CLVisc '''同目录下，从visc 文件中导入CLVivsc模块'''



def read_p4x4(cent='30_35', idx=0, 
        fname='/u/lpang/hdf5_data/auau200_run1.h5'):'''give a file named auau200'''
    '''read 4-momentum and 4-coordinates from h5 file,
    return: np.array with shape (num_of_partons, 8) return 9 numbers each row
    the first 4 columns store: E, px, py, pz
    the last 4 columns store: t, x, y, z '''
    with h5py.File(fname, 'r') as f:
        grp = f['cent']
        event_id = grp[cent][:, 0].astype(np.int)
        
        impact = grp[cent][:, 1] '''impact factor'''
        nw = grp[cent][:, 2]     '''number of wound particle'''
        nparton = grp[cent][:, 3] '''number of partons???'''
        key = 'event%s'%event_id[idx]
        #print
        p4x4 = f[key]
        return p4x4[...], event_id[idx], impact[idx], nw[idx], nparton[idx]
        
def event_by_event(fout, cent='30_35', idx=0, etaos=0.0,
                   fname_ini='/lustre/nyx/'
                   gpu_id = 0): '''定义一个逐事例的函数，输入的文件为Hdf5格式的文件'''
    ''' Run event_by_event hydro, with initial condition 
        from smearing on the particle list'''
        if not os.path.exists(fout):
            os.mkdir(fout)
        cfg.NX = 301
        cfg.NY = 301
        cfg.NZ = 5
        
        cfg.DT = 0.005
        cfg.DX = 0.1
        cfg.DY = 0.1
        cfg.DZ = 0.15
        cfg.IEOS = 1
        cfg.TFRZ = 0.136 '''Freezr-out termperature'''
        
        cfg.ntskip = 60
        cfg.nzskip = 1
        
        cfg.TAU0 = 0.4
        cfg.ETAOS = etaos
        cfg.fPathOut = fout
        
        t0 = time()
        visc = CLVsic(cfg, gpu_id=gpu_id)
        
        parton_list, eid, imp_b,  nwound, npartons = read_p4x4(cent, idx, fname_ini)
        
        comments = 'cent=%s, eventid=%s, impact parameter=%s, nw=%s, npartons=%s'%(
                cent, eid, imp_b, nwound, martons)
                
        write_config(cfg, comments)
        
        visc.smear_from_p4x4(parton_list, SIGR=0.6, SIGZ=0.6, KFACTOR=1.3, force_bjorken=Ture)
        
        visc.evolve(max_loops=4000, save_hypersf=True, save_bulk=True, save_vorticity=False)
        
        
        visc.queue.finish()
        
        t1 = time()
        print('finished. Total time: {dtime}'.format(dtime= t1 - t0))
        
        
        if __name__=='__main__':
            import sys
            if len(sys.argv) != 5:
                print("Usage: python ebe_bjorken.py collision_system centrality_range etaos gpuid")
                exit()
                
            collision_system = sys.argv[1]
            cent = sys.argv[2]
            etaos = np.float32(sys.argv[3])
            gpuid = int(sys.argv[4])
            
            path = '/lustre/nyx/hypid...'%(collision_system,
                    cent, etaos)
            path = path.replace('.', 'p')
            
            fname_ini='/lustre/nyx/hypid/lgpang/hdf5_data/%s.h5'%collision_system
            
            if not os.path.exists(path):
                os.makedirs(path)
                
            for idx in range(0, 100):
                fpath_out = path + 'event%s'%(idx)
                event_by_event(fpath_out, cent, idx, etaos=etaos, 
                              fname_ini=fname_ini, gpu_id=gpuid)
                              
                cwd = os.getcwd()
                os.chdir('../CLSmoothspec/build')
                #os.system
                #os.system
                call(['./spec',fpath_out])
                os.chdir(cwd)
                call(['python', '../spec/main.py', fpath_out])
            
            