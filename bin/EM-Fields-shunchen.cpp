// 2016
#include <stdlib.h>
#include <omp.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>
#include <iomanip>
#include <string>
// 头文件
#include "./parameter.h"
#include "./EM_fields.h"
#include "./gauss_quadrature.h"
// 用到的头文件，在同一目录下，有三个，分别是参数parameter.h, EM_fields.h, gauss_qudarature.h
using namespace std;

EM_fields::EM_fields(ParameterReader* paraRdr_in){ // 参数读入指针， paraRdr_in 指针
    initialization_status = 0; //初始状态 = 0
    paraRdr = paraRdr_in; //  参数读入设置为 paraRdr
    
    debug_flag = paraRdr->getVal("debug_flag"); // 指针指向getVal类？ debug 的语句
    mode = paraRdr->getVal("mode"); // 模式
    verbose_level = paraRdr->getVal("verbose_level"); // 波色能级？不懂
    turn_on_bulk = paraRdr->getVal("turn_on_bulk"); // 开启整体？
    include_participant_contributions = 
                        paraRdr->getVal("include_participant_contributions"); //参与碰撞粒子的贡献，指针
    int atomic_number = paraRdr->getVal("atomic_number"); // 原子核中核子数目；
    int number_of_proton = paraRdr->getVal("number_of_proton");
    charge_fraction = (static_cast<double>(number_of_proton)
                      /static_cast<double>(atomic_number));// 电荷分数=质子数/总核子数
    
    double ecm = paraRdr->getVal("ecm"); // set the number ecm?  
    double gamma = ecm/2./0.938;  // proton mass: 0.938 GeV
    double beta = sqrt(1. - 1./(gamma*gamma)); //与洛伦兹收缩因子gamma有关的因子
    double beam_rapidity = atanh(beta); // 快度 arctanh(beta)
    spectator_rap = beam_rapidity; // 旁观者快度 = 轴快度
    // cout << "spectator rapidity = " << spectator_rap << endl; // 输出轴快度
    
    nucleon_density_grid_size = paraRdr->getVal("nucleon_density_grid_size"); //原子核密度梯度的大小
    nucleon_density_grid_dx = paraRdr->getVal("nucleon_density_grid_dx");  // 核子密度梯度在 x 方向微元 dx 
    if (nucleon_density_grid_size <= 0) {  //如果核子密度梯度尺度小于等于0 ， 输出：错误，核子密度大小文件必须大于0，当前的梯度为=
        cout << "EM_fields:: Error: Grid size for nucleon density profiles "
             << "needs to be larger than 0!" << endl;
        cout << "Current grid_size = " << nucleon_density_grid_size << endl;
        exit(1);
    ｝
    nucleon_density_grid_x_array = new double[nucleon_density_grid_size]; // 核子密度梯度在x方向 = double 类型的数
    nucleon_density_grid_y_array = new double[nucleon_density_grid_size]; // 同上，y 方向
    spectator_density_1 = new double* [nucleon_density_grid_size];  // 旁观子_1的密度， 用double 类型进行存储
    spectator_density_2 = new double* [nucleon_density_grid_size];  // 旁观子_2的密度， 用double 类型进行存储
    participant_density_1 = new double* [nucleon_density_grid_size]; // 参与子的密度
    participant_density_2 = new double* [nucleon_density_grid_size];
    for (int i = 0; i < nucleon_density_grid_size; i++) { // for 循环，最大为核子梯度方向大小，每次加1
        nucleon_density_grid_x_array[i] = ( 
            (-(nucleon_density_grid_size-1)/2. + i)*nucleon_density_grid_dx); //核子在x方向第i个核子密度序列 = -(（核子密度梯度大小-1）/2 + i)*dx,简单表达取值 [-N/2 *dx，N/2 *dx] 
        nucleon_density_grid_y_array[i] = (
            (-(nucleon_density_grid_size-1)/2. + i)*nucleon_density_grid_dx); // y方向第i个核子密度序列
        spectator_density_1[i] = new double[nucleon_density_grid_size]; // 旁观子1第i个的密度 = 新的双精度变量，大小为[核子密度梯度大小]
        spectator_density_2[i] = new double[nucleon_density_grid_size];
        participant_density_1[i] = new double[nucleon_density_grid_size];
        participant_density_2[i] = new double[nucleon_density_grid_size];
        for (int j = 0; j < nucleon_density_grid_size; j++) {
            spectator_density_1[i][j] = 0.0; // 旁观者的位置及其对应的矩阵，矩阵大小为 i*j，矩阵元设为0.0
            spectator_density_2[i][j] = 0.0;
            participant_density_1[i][j] = 0.0;
            participant_density_2[i][j] = 0.0;
        ｝
    }
    
    n_eta = paraRdr->getVal("n_eta"); // n_eta 大小来自指针 getVal n_eta
    eta_grid = new double[n_eta];  // 设置 eta_grid 变量
    sinh_eta_array = new double[n_eta]; // sinh_eta_array = 新的double 变量，大小为 n_eta
    cosh_eta_array = new double[n_eta]; // cosh_eta_array
    double deta = 1.0; // double eta的微元
    if (n_eta > 1) {
        deta = 2.*beam_rapidity*0.99/(n_eta - 1.); // (若eta>1, deta = 2*横快度*0.99/（neta-1.）);
        for (int i = 0; i < n_eta; i++) {
            eta_grid[i] = - beam_rapidity*0.99 + i*deta;
            sinh_eta_array[i] = sinh(eta_grid[i]);
            cosh_eta_array[i] = cosh(eta_grid[i]); // 利用for 循环得到 sinh eta 以及 cosh eta 的大小
        }
    } else {
        eta_grid[0] = 0.0;
        sinh_eta_array[0] = sinh(eta_grid[0]);
        cosh_eta_array[0] = cosh(eta_grid[0]);
    }
    
    read_in_densities("./results"); // 读取的函数， 读取当前目录下的result文件夹
    
    if (mode == 0) {
        set_4d_grid_points(); // mode =0, 4维梯度点
    } else if (mode == 1) { // mode =1, 读取freezeout surface points VISH2p1 的数据
        read_in_freezeout_surface_points_VISH2p1("./results/surface.dat",
                                                 "./results/decdat2.dat");
    } else if (mode == 2) { //  mode =2, 设置 tau的grid 点 0.0， 0.0，0.0
        set_tau_grid_points(0.0, 0.0, 0.0);
    } else if (mode == 3) { // mode =3 是读取VISH2P1 boost invariant 的数据
        read_in_freezeout_surface_points_VISH2p1_boost_invariant(
                                                    "./results/surface.dat");
    } else if (mode == 4) { // mode =4 是读取MUSIC的数据
        read_in_freezeout_surface_points_MUSIC("./results/surface.dat");
    } else if (mode == -1) { // mode=-1是读取 Gubser的数据
        read_in_freezeout_surface_points_Gubser("./results/surface.dat");
    } else { // 除去以上 0，1，2，3，4，-1六种模式外， 其它的都输出ERROR
        cout << "EM_fields:: Error: unrecognize mode! "
             << "mode = " << mode << endl;
        exit(1);
    }

    initialization_status = 1; // 初始的状态 = 1
}

EM_fields::~EM_fields() {
    if (initialization_status == 1) {
        for (int i = 0; i < nucleon_density_grid_size; i++) {
            delete[] spectator_density_1[i];
            delete[] spectator_density_2[i];
            delete[] participant_density_1[i];
            delete[] participant_density_2[i];
        }
        delete[] spectator_density_1;
        delete[] spectator_density_2;
        delete[] participant_density_1;
        delete[] participant_density_2;
        delete[] nucleon_density_grid_x_array;
        delete[] nucleon_density_grid_y_array;

        delete[] eta_grid;
        delete[] sinh_eta_array;
        delete[] cosh_eta_array;

        cell_list.clear();
    }
    return;
} // 保证每次的核子密度大小，旁观者，参与者，eta, sinh cosh 数据是干净的

void EM_fields::read_in_densities(string path) { // EM_fields 读入旁观与参与子的密度函数， 如果mode = -1, 读入当前目录下的spectator_density_A_disk.dat 以及spectator_density_B_disk.dat两个文件
    // spectators
    ostringstream spectator_1_filename;
    ostringstream spectator_2_filename;
    if (mode == -1) {
        spectator_1_filename << path
                             << "/spectator_density_A_disk.dat";
        spectator_2_filename << path
                             << "/spectator_density_B_disk.dat";
    } else {
        spectator_1_filename << path // 否则，读入 spectator_density_A_fromSd_order_2.dat 
                             << "/spectator_density_A_fromSd_order_2.dat";
        spectator_2_filename << path
                             << "/spectator_density_B_fromSd_order_2.dat";
    }
    read_in_spectators_density(spectator_1_filename.str(),
                               spectator_2_filename.str());
    // participants
    if (include_participant_contributions == 1) { // 如果 include_participant_contribution ==1 则读入核子厚度函数TA 和核子厚度函数TB的数据
        ostringstream participant_1_filename;
        participant_1_filename << path 
                               << "/nuclear_thickness_TA_fromSd_order_2.dat";
        ostringstream participant_2_filename;
        participant_2_filename << path 
                               << "/nuclear_thickness_TB_fromSd_order_2.dat";
        read_in_participant_density(participant_1_filename.str(), 
                                    participant_2_filename.str());
    }
}

void EM_fields::read_in_spectators_density(string filename_1,
                                           string filename_2) {
    if (verbose_level > 3) {   // verbose_level >3 是什么？ 查询头文件！
        cout << "read in spectator density ..."; //  读入旁观者的密度
    }
    ifstream spec1(filename_1.c_str());
    ifstream spec2(filename_2.c_str());
    if (!spec1.good()) {
        cout << "Error:EM_fields::read_in_spectators_density: "
             << "can not open file " << filename_1 << endl;
        exit(1);
    }
    if (!spec2.good()) {
        cout << "Error:EM_fields::read_in_spectators_density: "
             << "can not open file " << filename_2 << endl;
        exit(1);
    }

    for (int i = 0; i < nucleon_density_grid_size; i++) {  // 做循环，最大为核子密度梯度大小
        for (int j = 0; j < nucleon_density_grid_size; j++) {
            spec1 >> spectator_density_1[i][j];
            spec2 >> spectator_density_2[i][j]; // 读入两个旁观子的密度矩阵，大小为 nucleon_density_grid_size**2
        }
    }

    spec1.close();
    spec2.close();
    if (verbose_level > 3) {
        cout << " done!" << endl;
    }
}

void EM_fields::read_in_participant_density(string filename_1,
                                            string filename_2) {
    if (verbose_level > 3) {
        cout << "read in participant density ...";
    }
    ifstream part1(filename_1.c_str());
    ifstream part2(filename_2.c_str());
    if (!part1.good()) {
        cout << "Error:EM_fields::read_in_participant_density: "
             << "can not open file " << filename_1 << endl;
        exit(1);
    }
    if (!part2.good()) {
        cout << "Error:EM_fields::read_in_participant_density: "
             << "can not open file " << filename_2 << endl;
        exit(1);
    }

    for (int i = 0; i < nucleon_density_grid_size; i++) {
        for (int j = 0; j < nucleon_density_grid_size; j++) {
            part1 >> participant_density_1[i][j];
            part2 >> participant_density_2[i][j];
        }
    }

    part1.close();
    part2.close();
    if (verbose_level > 3) {
        cout << " done!" << endl;
    }
}

void EM_fields::set_tau_grid_points(double x_local, double y_local,  // EM_fields函数中的set_tau_grid_points函数，共三个变量 x,y,eta_local
                                    double eta_local) {
    double EM_fields_grid_size = 15.0; // grid_size = 15.0
    double EM_fields_grid_dtau = 0.01; // grid_dtau = 0.01

    int number_of_points =
                static_cast<int>(EM_fields_grid_size/EM_fields_grid_dtau) + 1; // 点的数目，统计点 EM_fields_grid_size/EM_fields_grid_dtau + 1
    for (int i = 0; i < number_of_points; i++) {  // 做循环 i < number_of_points 
        double tau_local = 0.0 + i*EM_fields_grid_dtau;  // double tau_local = 0.0 + i* EM_fields_grid_dtau
        fluidCell cell_local; // fluidCell 是 cell_local
        cell_local.tau = tau_local; // tau 的值
        cell_local.x = x_local; // x
        cell_local.y = y_local; // y
        cell_local.eta = eta_local; // 
        cell_list.push_back(cell_local);
    }
    EM_fields_array_length = cell_list.size();
    if (verbose_level > 1) {
        cout << "number of freeze-out cells: "
             << EM_fields_array_length << endl;
    }
}

void EM_fields::set_4d_grid_points() { //函数 EM::fields中的子函数，设置4维grid 点
    cell_list.clear();
    double EM_fields_grid_size = 20.0; // /EM_fields_grid_size =20.0 电磁场尺寸
    double EM_fields_grid_dx = 0.5; // x方向上的大小
    double EM_fields_grid_neta = 41; // neta 的数目
    double EM_fields_grid_ntau = 11; // ntau 的数目
    double EM_fields_grid_tau_max = 5.0; 
    int number_of_points =
                static_cast<int>(EM_fields_grid_size/EM_fields_grid_dx) + 1; // 整数型的 EM 场梯度大小
    double EM_fields_grid_deta = 2.*spectator_rap/(EM_fields_grid_neta - 1); // 2*eta/(41-1),eta的步长
    double EM_fields_grid_dtau =
                EM_fields_grid_tau_max/(EM_fields_grid_ntau - 1);//dtau = 5.0/（11-1）dtau的步长
    for (int l = 0; l < EM_fields_grid_ntau; l++) { //对  tau 做循环 
        double tau_local = 0.0 + l*EM_fields_grid_dtau; // tau ~[ 0.0, 5.0, 0.5 ]
        for (int k = 0; k < EM_fields_grid_neta; k++) { // 对 eta 做循环 
            double eta_local = -spectator_rap + k*EM_fields_grid_deta; // eta 值 
            for (int i = 0; i < number_of_points; i++) {
                double x_local = - EM_fields_grid_size/2. + i*EM_fields_grid_dx; // x
                for (int j = 0; j < number_of_points; j++) {
                    double y_local = - EM_fields_grid_size/2. + j*EM_fields_grid_dx; // y
                    fluidCell cell_local;
                    cell_local.tau = tau_local; // 定义 tau x y eta, 为什么不是  tau x_{perp} phi eta?
                    cell_local.x = x_local;
                    cell_local.y = y_local;
                    cell_local.eta = eta_local;
                    cell_local.mu_m = M_PI/2.*sqrt(6*M_PI)*0.2*0.2;  // GeV^2 四速度 beta ?
                    cell_local.beta.x = 0.0;
                    cell_local.beta.y = 0.0;
                    cell_local.beta.z = tanh(eta_local); //快度
                    cell_list.push_back(cell_local);
                }
            }
        }
    }
    EM_fields_array_length = cell_list.size();
    if (verbose_level > 1) {
        cout << "number of freeze-out cells: "
             << EM_fields_array_length << endl;
    }
}

void EM_fields::read_in_freezeout_surface_points_VISH2p1(string filename1,
                                                         string filename2) {//读取来自VISH2p1的超曲面信息
    // this function reads in the freeze out surface points from a text file
    ifstream FOsurf(filename1.c_str());
    ifstream decdat(filename2.c_str());
    if (verbose_level > 1) {
        cout << "read in freeze-out surface points from VISH2+1 outputs ...";
    }
    // read in freeze-out surface positions
    double dummy;
    string input;//读取的信息有 tau_local, x, y, vx, vy, T
    double tau_local, x_local, y_local;
    double vx_local, vy_local;
    double T_local;
    FOsurf >> dummy;
    while (!FOsurf.eof()) {
        FOsurf >> tau_local >> x_local >> y_local >> dummy >> dummy >> dummy;
        getline(decdat, input, '\n');
        stringstream ss(input);
        ss >> dummy >> dummy >> dummy >> dummy;     // skip tau and da_i
        ss >> vx_local >> vy_local;                 // read in vx and vy
        ss >> dummy >> dummy >> T_local;            // read in temperature
        // the rest is discharded
        double u_tau_local = 1./sqrt(1. - vx_local*vx_local
                                     - vy_local*vy_local); // 计算固有时
        double u_x_local = u_tau_local*vx_local; // 四速度 vx vy
        double u_y_local = u_tau_local*vy_local; 
        for (int i = 0; i < n_eta; i++) {
            fluidCell cell_local;
            cell_local.mu_m = M_PI/2.*sqrt(6*M_PI)*T_local*T_local;  // GeV^2
            cell_local.eta = eta_grid[i];
            cell_local.tau = tau_local;
            cell_local.x = x_local;
            cell_local.y = y_local;

            // compute fluid velocity in t-xyz coordinate
            double u_t_local = u_tau_local*cosh_eta_array[i]; 
            double u_z_local = u_tau_local*sinh_eta_array[i];
            cell_local.beta.x = u_x_local/u_t_local;
            cell_local.beta.y = u_y_local/u_t_local;
            cell_local.beta.z = u_z_local/u_t_local;

            // push back the fluid cell into the cell list
            cell_list.push_back(cell_local);
        }
        FOsurf >> dummy;
    }
    FOsurf.close();
    decdat.close();
    if (verbose_level > 1) {
        cout << " done!" << endl;
    }
    EM_fields_array_length = cell_list.size();
    if (verbose_level > 1) {
        cout << "number of freeze-out cells: "
             << EM_fields_array_length << endl;
    }
}

void EM_fields::read_in_freezeout_surface_points_Gubser(string filename) { // 读取 Gubser 速度
    // this function reads in the freeze out surface points from a text file
    ifstream FOsurf(filename.c_str());
    if (verbose_level > 1) {
        cout << "read in freeze-out surface points from Gubser ...";
    }
    if (!FOsurf.good()) {
        cout << "Error:EM_fields::"
             << "read_in_freezeout_surface_points_Gubser:"
             << "can not open file: " << filename << endl;
        exit(1);
    }

    // read in freeze-out surface positions
    string input;
    double tau_local, x_local, y_local;
    double u_tau_local, u_x_local, u_y_local;
    double T_local;
    getline(FOsurf, input, '\n');  // read in header
    getline(FOsurf, input, '\n');
    while (!FOsurf.eof()) {
        stringstream ss(input);
        ss >> x_local >> tau_local >> u_tau_local >> u_x_local;
        y_local = 0.0;
        u_y_local = 0.0;
        T_local = 0.255;  // GeV
        fluidCell cell_local;
        cell_local.mu_m = M_PI/2.*sqrt(6*M_PI)*T_local*T_local;  // GeV^2
        if (cell_local.mu_m < 1e-5) {     // mu_m is too small
            cout << cell_local.mu_m << "  " << T_local << endl;
            exit(1);
        }
        cell_local.eta = 0.0;
        cell_local.tau = tau_local;
        cell_local.x = x_local;
        cell_local.y = y_local;

        // compute fluid velocity in t-xyz coordinate
        double u_t_local = u_tau_local;
        double u_z_local = 0.0;
        cell_local.beta.x = u_x_local/u_t_local;
        cell_local.beta.y = u_y_local/u_t_local;
        cell_local.beta.z = u_z_local/u_t_local;

        // push back the fluid cell into the cell list
        cell_list.push_back(cell_local);
        getline(FOsurf, input, '\n');
    }
    FOsurf.close();
    if (verbose_level > 1) {
        cout << " done!" << endl;
    }
    EM_fields_array_length = cell_list.size();
    if (verbose_level > 1) {
        cout << "number of freeze-out cells: "
             << EM_fields_array_length << endl;
    }
}

void EM_fields::read_in_freezeout_surface_points_VISH2p1_boost_invariant(
                                                            string filename) {
    // this function reads in the freeze out surface points from a text file
    ifstream FOsurf(filename.c_str());
    if (verbose_level > 1) {
        cout << "read in freeze-out surface points from VISH2+1 "
             << "boost invariant outputs ...";
    }
    if (!FOsurf.good()) {
        cout << "Error:EM_fields::"
             << "read_in_freezeout_surface_points_VISH2p1_boost_invariant:"
             << "can not open file: " << filename << endl;
        exit(1);
    }

    // read in freeze-out surface positions
    double dummy;
    string input;
    double tau_local, x_local, y_local;
    double u_tau_local, u_x_local, u_y_local;
    double T_local;
    getline(FOsurf, input, '\n');
    while (!FOsurf.eof()) {
        stringstream ss(input);
        ss >> tau_local >> x_local >> y_local >> dummy;  // eta_s = 0.0
        ss >> dummy >> dummy >> dummy >> dummy;   // skip surface vector da_mu
        // read in flow velocity
        ss >> dummy >> u_x_local >> u_y_local >> dummy;  // u_eta = 0.0
        u_tau_local = sqrt(1. + u_x_local*u_x_local + u_y_local*u_y_local);
        ss >> dummy >> dummy >> T_local;
        // the rest information is discarded
        for (int i = 0; i < n_eta; i++) {
            fluidCell cell_local;
            cell_local.mu_m = M_PI/2.*sqrt(6*M_PI)*T_local*T_local;  // GeV^2
            if (cell_local.mu_m < 1e-5) {     // mu_m is too small
                cout << cell_local.mu_m << "  " << T_local << endl;
                exit(1);
            }
            cell_local.eta = eta_grid[i];
            cell_local.tau = tau_local;
            cell_local.x = x_local;
            cell_local.y = y_local;

            // compute fluid velocity in t-xyz coordinate
            double u_t_local = u_tau_local*cosh_eta_array[i];
            double u_z_local = u_tau_local*sinh_eta_array[i];
            cell_local.beta.x = u_x_local/u_t_local;
            cell_local.beta.y = u_y_local/u_t_local;
            cell_local.beta.z = u_z_local/u_t_local;

            // push back the fluid cell into the cell list
            cell_list.push_back(cell_local);
        }
        getline(FOsurf, input, '\n');
    }
    FOsurf.close();
    if (verbose_level > 1) {
        cout << " done!" << endl;
    }
    EM_fields_array_length = cell_list.size();
    if (verbose_level > 1) {
        cout << "number of freeze-out cells: "
             << EM_fields_array_length << endl;
    }
}

void EM_fields::read_in_freezeout_surface_points_MUSIC(string filename) {
    // this function reads in the freeze out surface points from a text file
    ifstream FOsurf(filename.c_str());
    if (verbose_level > 1) {
        cout << "read in freeze-out surface points from MUSIC "
             << "(3+1)-d outputs ...";
    }
    if (!FOsurf.good()) {
        cout << "Error:EM_fields::"
             << "read_in_freezeout_surface_points_MUSIC:"
             << "can not open file: " << filename << endl;
        exit(1);
    }

    // read in freeze-out surface positions
    double dummy;
    string input;
    double tau_local, x_local, y_local, eta_s_local;
    double u_tau_local, u_x_local, u_y_local, u_eta_local;
    double T_local;
    getline(FOsurf, input, '\n');
    while (!FOsurf.eof()) {
        stringstream ss(input);
        ss >> tau_local >> x_local >> y_local >> eta_s_local;
        ss >> dummy >> dummy >> dummy >> dummy;
        // read in flow velocity
        // here u^eta = tau*u^eta
        ss >> dummy >> u_x_local >> u_y_local >> u_eta_local;
        u_tau_local = sqrt(1. + u_x_local*u_x_local + u_y_local*u_y_local
                           + u_eta_local*u_eta_local);
        ss >> dummy >> T_local;
        // the rest information is discarded
        fluidCell cell_local;
        cell_local.mu_m = M_PI/2.*sqrt(6*M_PI)*T_local*T_local;  // GeV^2
        if (cell_local.mu_m < 1e-5) {     // mu_m is too small
            cout << cell_local.mu_m << "  " << T_local << endl;
            exit(1);
        }
        cell_local.eta = eta_s_local;
        cell_local.tau = tau_local;
        cell_local.x = x_local;
        cell_local.y = y_local;

        // compute fluid velocity in t-xyz coordinate
        double cosh_eta_s = cosh(eta_s_local);
        double sinh_eta_s = sinh(eta_s_local);
        double u_t_local = u_tau_local*cosh_eta_s + u_eta_local*sinh_eta_s;
        double u_z_local = u_tau_local*sinh_eta_s + u_eta_local*cosh_eta_s;
        cell_local.beta.x = u_x_local/u_t_local;
        cell_local.beta.y = u_y_local/u_t_local;
        cell_local.beta.z = u_z_local/u_t_local;

        // push back the fluid cell into the cell list
        cell_list.push_back(cell_local);
        getline(FOsurf, input, '\n');
    }
    FOsurf.close();
    if (verbose_level > 1) {
        cout << " done!" << endl;
    }
    EM_fields_array_length = cell_list.size();
    if (verbose_level > 1) {
        cout << "number of freeze-out cells: "
             << EM_fields_array_length << endl;
    }
}
// 至此，程序计算了读入四种不同的超曲面的程序
void EM_fields::calculate_EM_fields() {// 计算电磁场 EM，用 解析解 进行计算
    int i_array; // i
    int count = 0; 
    #pragma omp parallel private(i_array, count) //CPU 并行运算
    {
    if (omp_get_thread_num() == 0) {
        cout << "computing EM fields with " << omp_get_num_threads()
             << " cpu cores..." << endl;
    }
    #pragma omp for
    for (i_array = 0; i_array < EM_fields_array_length; i_array++) {
        double sigma = 0.023;       // electric conductivity [fm^-1]
        double cosh_spectator_rap = cosh(spectator_rap); 
        double sinh_spectator_rap = sinh(spectator_rap);

        double participant_coeff_a = 0.5;
        double participant_rapidity_envelop_coeff =
            participant_coeff_a/(2.*sinh(participant_coeff_a*spectator_rap));
        int participant_rapidity_integral_ny = 50;
        double *participant_rap_inte_y_array =
                                new double[participant_rapidity_integral_ny];
        double *participant_rap_inte_weight_array =
                                new double[participant_rapidity_integral_ny];
        gauss_quadrature(participant_rapidity_integral_ny, 1, 0.0, 0.0,
                         -spectator_rap, spectator_rap,
                         participant_rap_inte_y_array,
                         participant_rap_inte_weight_array);

        double dx_sq = nucleon_density_grid_dx*nucleon_density_grid_dx;

        double field_x = cell_list[i_array].x;
        double field_y = cell_list[i_array].y;
        double field_tau = cell_list[i_array].tau;
        double field_eta = cell_list[i_array].eta;
        double temp_sum_Ex_spectator = 0.0e0;
        double temp_sum_Ey_spectator = 0.0e0;
        double temp_sum_Ez_spectator = 0.0e0;
        double temp_sum_Bx_spectator = 0.0e0;
        double temp_sum_By_spectator = 0.0e0;

        double z_local_spectator_1 = field_tau*sinh(spectator_rap - field_eta);
        double z_local_spectator_2 = (
                                field_tau*sinh(-spectator_rap - field_eta));
        double z_local_spectator_1_sq = (z_local_spectator_1
                                         *z_local_spectator_1);
        double z_local_spectator_2_sq = (z_local_spectator_2
                                         *z_local_spectator_2);

        for (int i = 0; i < nucleon_density_grid_size; i++) {
            double grid_x = nucleon_density_grid_x_array[i];
            for (int j = 0; j < nucleon_density_grid_size; j++) {
                double grid_y = nucleon_density_grid_y_array[j];
                double x_local = field_x - grid_x;
                double y_local = field_y - grid_y;
                double r_perp_local_sq = x_local*x_local + y_local*y_local;
                double Delta_1 = sqrt(r_perp_local_sq
                                      + z_local_spectator_1_sq);
                double Delta_1_cubic = Delta_1*Delta_1*Delta_1;
                double Delta_2 = sqrt(r_perp_local_sq
                                      + z_local_spectator_2_sq);
                double Delta_2_cubic = Delta_2*Delta_2*Delta_2;
                double A_1 = (sigma/2.*(z_local_spectator_1 - Delta_1)
                                      *sinh_spectator_rap);
                double A_2 = (sigma/2.*(z_local_spectator_2 + Delta_2)
                                      *(-sinh_spectator_rap));
                double exp_A_1 = exp(A_1);
                double exp_A_2 = exp(A_2);
                double common_integrand_E = (
                    spectator_density_1[i][j]/(Delta_1_cubic + 1e-15)
                      *(sigma/2.*sinh_spectator_rap*Delta_1 + 1.)*exp_A_1
                    + spectator_density_2[i][j]/(Delta_2_cubic + 1e-15)
                      *(sigma/2.*sinh_spectator_rap*Delta_2 + 1.)*exp_A_2
                );
                double common_integrand_B = (
                    spectator_density_1[i][j]/(Delta_1_cubic + 1e-15)
                    *(sigma/2.*sinh_spectator_rap*Delta_1 + 1.)*exp_A_1
                    - spectator_density_2[i][j]/(Delta_2_cubic + 1e-15)
                      *(sigma/2.*sinh_spectator_rap*Delta_2 + 1.)*exp_A_2
                );

                double Ex_integrand = x_local*common_integrand_E;
                double Ey_integrand = y_local*common_integrand_E;
                double Bx_integrand = -y_local*common_integrand_B;
                double By_integrand = x_local*common_integrand_B;
                
                temp_sum_Ex_spectator += Ex_integrand;
                temp_sum_Ey_spectator += Ey_integrand;
                temp_sum_Bx_spectator += Bx_integrand;
                temp_sum_By_spectator += By_integrand;
            }
        }
        
        // compute contribution from participants
        double temp_sum_Ex_participant = 0.0e0;
        double temp_sum_Ey_participant = 0.0e0;
        double temp_sum_Ez_participant = 0.0e0;
        double temp_sum_Bx_participant = 0.0e0;
        double temp_sum_By_participant = 0.0e0;
        
        if (include_participant_contributions == 1) {
            for (int k = 0; k < participant_rapidity_integral_ny; k++) {
                double rap_local = participant_rap_inte_y_array[k];
                double sinh_participant_rap = sinh(rap_local);
                double cosh_participant_rap = cosh(rap_local);

                double exp_participant_rap_1 =
                                        exp(participant_coeff_a*rap_local);
                double exp_participant_rap_2 = exp_participant_rap_1;
                double z_local_participant_1 =
                                    field_tau*sinh(rap_local - field_eta);
                double z_local_participant_2 = (
                                    field_tau*sinh(-rap_local - field_eta));
                double z_local_participant_1_sq = (z_local_participant_1
                                                   *z_local_participant_1);
                double z_local_participant_2_sq = (z_local_participant_2
                                                   *z_local_participant_2);

                double Ex_integrand = 0.0;
                double Ey_integrand = 0.0;
                double Bx_integrand = 0.0;
                double By_integrand = 0.0;
                for (int i = 0; i < nucleon_density_grid_size; i++) {
                    double grid_x = nucleon_density_grid_x_array[i];
                    for (int j = 0; j < nucleon_density_grid_size; j++) {
                        double grid_y = nucleon_density_grid_y_array[j];
                        double x_local = field_x - grid_x;
                        double y_local = field_y - grid_y;
                        double r_perp_local_sq = (
                                        x_local*x_local + y_local*y_local);
                        double Delta_1 = sqrt(r_perp_local_sq
                                              + z_local_participant_1_sq);
                        double Delta_1_cubic = Delta_1*Delta_1*Delta_1;
                        double Delta_2 = sqrt(r_perp_local_sq
                                              + z_local_participant_2_sq);
                        double Delta_2_cubic = Delta_2*Delta_2*Delta_2;
                        double A_1 = (sigma/2.
                                *(z_local_participant_1*sinh_participant_rap
                                  - fabs(sinh_participant_rap)*Delta_1));
                        double A_2 = (sigma/2.
                                *(z_local_participant_2*(-sinh_participant_rap)
                                  - fabs(-sinh_participant_rap)*Delta_2));
                        double exp_A_1 = exp(A_1);
                        double exp_A_2 = exp(A_2);
                        double common_integrand_E = (
                            (participant_density_1[i][j]
                             /(Delta_1_cubic + 1e-15)
                             *(sigma/2.*fabs(sinh_participant_rap)*Delta_1 + 1.)
                             *exp_A_1)*exp_participant_rap_1
                          + (participant_density_2[i][j]
                             /(Delta_2_cubic + 1e-15)
                             *(sigma/2.*fabs(sinh_participant_rap)*Delta_2 + 1.)
                             *exp_A_2)*exp_participant_rap_2);
                        double common_integrand_B = (
                            (participant_density_1[i][j]
                             /(Delta_1_cubic + 1e-15)
                             *(sigma/2.*fabs(sinh_participant_rap)*Delta_1 + 1.)
                             *exp_A_1)*exp_participant_rap_1
                          - (participant_density_2[i][j]
                             /(Delta_2_cubic + 1e-15)
                             *(sigma/2.*fabs(sinh_participant_rap)*Delta_2 + 1.)
                             *exp_A_2)*exp_participant_rap_2);

                        Ex_integrand += x_local*common_integrand_E;
                        Ey_integrand += y_local*common_integrand_E;
                        Bx_integrand += -y_local*common_integrand_B;
                        By_integrand += x_local*common_integrand_B;
                    }
                }
                temp_sum_Ex_participant += (Ex_integrand*cosh_participant_rap
                                        *participant_rap_inte_weight_array[k]);
                temp_sum_Ey_participant += (Ey_integrand*cosh_participant_rap
                                        *participant_rap_inte_weight_array[k]);
                temp_sum_Bx_participant += (Bx_integrand*sinh_participant_rap
                                        *participant_rap_inte_weight_array[k]);
                temp_sum_By_participant += (By_integrand*sinh_participant_rap
                                        *participant_rap_inte_weight_array[k]);
            }
        }

        cell_list[i_array].E_lab.x = (charge_fraction*alpha_EM
            *(temp_sum_Ex_spectator*cosh_spectator_rap
              + temp_sum_Ex_participant*participant_rapidity_envelop_coeff
             )*dx_sq);
        cell_list[i_array].E_lab.y = (charge_fraction*alpha_EM
            *(temp_sum_Ey_spectator*cosh_spectator_rap
              + temp_sum_Ey_participant*participant_rapidity_envelop_coeff
             )*dx_sq);
        cell_list[i_array].E_lab.z = (charge_fraction*alpha_EM
            *(temp_sum_Ez_spectator
              + temp_sum_Ez_participant*participant_rapidity_envelop_coeff
             )*dx_sq);
        cell_list[i_array].B_lab.x = (charge_fraction*alpha_EM
            *(temp_sum_Bx_spectator*sinh_spectator_rap
              + temp_sum_Bx_participant*participant_rapidity_envelop_coeff
             )*dx_sq);
        cell_list[i_array].B_lab.y = (charge_fraction*alpha_EM
            *(temp_sum_By_spectator*sinh_spectator_rap
              + temp_sum_By_participant*participant_rapidity_envelop_coeff
             )*dx_sq);
        cell_list[i_array].B_lab.z = 0.0;

        // convert units to [GeV^2]
        cell_list[i_array].E_lab.x *= hbarCsq;
        cell_list[i_array].E_lab.y *= hbarCsq;
        cell_list[i_array].E_lab.z *= hbarCsq;
        cell_list[i_array].B_lab.x *= hbarCsq;
        cell_list[i_array].B_lab.y *= hbarCsq;
        cell_list[i_array].B_lab.z *= hbarCsq;

        if (verbose_level > 3) {
            if (omp_get_thread_num() == 0) {
                count++;
                int total_num_cells = static_cast<int>(EM_fields_array_length
                                                       /omp_get_num_threads());
                if (count % static_cast<int>(total_num_cells/10) == 0) {
                    cout << "computing EM fields: " << setprecision(3)
                         << (static_cast<double>(count)
                             /static_cast<double>(total_num_cells)*100)
                         << "\% done." << endl;
                }
            }
        }
        // clean up
        delete[] participant_rap_inte_y_array;
        delete[] participant_rap_inte_weight_array;
    }
    #pragma omp barrier
    }
    return;
}

