#/usr/bin/env python
#author
#e-mail
#create: 2019
''' calc the magnetic response of the QGP 
with fluid velocity given by hydrodynamic simulations'''

import matplotlib.pyploe as plt # import the plot 
import numpy as np # numpy
import sympy as sym # sys
from math import floor # math floor, 取整
#import pyopencl as cl

import os, sys
cwd, cwf  = os.path.split(__file__)
sys.path.append(os.path.join(cwd, '../pyvisc'))
from config import cfg # 导入 config 模块中的 cfg
from config import write_config # 
#from visc import CLVisc
#from ideal import CLIdeal

from common_plotting import smash_style

class MagneticField(object):
    def __init__(self, eB0, sigx, sigy, hydro_cfg, bulkinfo=None, sigma=5.8):
        '''eB0: maximum magnetic field
           sigx: gaussian width of magnetic field along x
           sigy: gaussian width of magnetic field along y
           nx, ny: grids along x and y direction
           dx, dy: space step along x and y direction
           hydro_dir: directory with fluid velocity profile
           sigma: electric conductivity in units of MeV '''
        nx, ny = hydro_cfg.NX, hydro_cfg.NY
        dx, dy = hydro_cfg.DX, hydro_cfg.DY
        dt = hydro_cfg.DT * hydro_cfg.ntskip
        self.hydro_dir = hydro_cfg.fPathOut
        
        x = np.linspace(-floor(nx/2)*dx, floor(nx/2)*dx, nx, endpoit=True)
        y = np.linspace(-floor(ny/2)*dy, floor(nx/2)*dy, ny, endpoit=True)
        self.x, self.y = x, y
        
        # for gridients and dB/dt calculation
        self.dx, self.dy, self.dt = dx, dy, dt
        
        x, y = np.meshgrid(x, y, indexing='ij')
        
        #By0 = eB0 * exp(-x*x/(2*sigx*sigx)-y*y/(2*sigy*sigy))
        By0 = eB0 * sigx*sigx/(x*x + sigx*sigx)* np.exp(-y*y/(2*sigy*sigy))
        Bx0 = eB0 * sigx*np.arctan(x/sigx) * y/(sigy * sigy) * np.exp(-y*y/(2*sigy*sigy))
        #Bx0 = np.zeros_like(By0)
        Bz0 = np.zeros_like(By0)
        
        self.B0 = [Bx0, By0, Bz0]
        
        #converts MeV to fm^{-1}
        self.sigma = sigma * 0.001/0.19732
        
        self.hydro_cfg = hydro_cfg
        self.bulkinfo = bulkinfo
        
        #self.B stores [[Bx, By, Bz], ... array
        self.B = []
        
    def v_cross_B(self, v, B):
        ''' v cross B '''
        a = v[1]*B[2] - v[2]*B[1]
        b = -v[0]*B[2] + v[2]*B[0]
        c = v[0]*B[1] - v[1]*B[0]
        return np.array([a, b, c])
        
    def curl(self, B):
        ''' return: nabla X B '''
        dBx = np.gradient(B[0])
        dBy = np.gradient(B[1])
        dBz = np.gradient(B[2])
        zeros = np.zeros_like(B[0])
        dyBz, dzBy = dBz[1]/self.dy, zeros
        dxBz, dzBx = dBz[0]/self.dx, zeros
        dxBy, dyBx = dBy[0]/self.dx, dBx[1]/self.dy
        curl_x = dyBz - dzBy
        curl_y = dzBx - dxBz
        curl_z = dxBy - dyBx
        return np.array([curl_x, curl_y, curl_z])
        
    def electric_field(self, E0, v0, B0, v1, B1, dt, sigma)：
        ''' No RungeKutta: E^n+1 = (En + dt*(curl B - sigma vXB)
        